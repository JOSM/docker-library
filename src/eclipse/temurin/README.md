# `eclipse-temurin` image

See the tags in the [Container Registry](https://gitlab.com/JOSM/docker-library/container_registry/6753857).

> **Note:**
> In the GitLab UI the images are shown as being zero Bytes in size. That is a bug in Gitlab, see https://gitlab.com/gitlab-org/gitlab/-/issues/431048

## What's in the image

* based on [`eclipse-temurin:x-noble`](https://hub.docker.com/_/eclipse-temurin) (where x is one of 8, 11, 17 or 21)
* add some modified default [Gradle properties](./21/gradle.properties), see [the Gradle docs](https://docs.gradle.org/8.10/userguide/build_environment.html#sec:gradle_configuration_properties) for details about the different settings
* install some packages via `apt-get`:
  * **only for Java 8:**`openjfx` for JavaFX support
  * `git`
  * `openssh`
  * `gettext` for working with i18n files
  * `fonts-dejavu` to avoid [Java problems with font metrics](https://stackoverflow.com/q/8109607)
* the domains `github.com` and `gitlab.com` are added to `~/.ssh/known_hosts`
