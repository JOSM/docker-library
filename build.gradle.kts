val DOCKER_REGISTRY: String = System.getenv("CI_REGISTRY") ?: "registry.gitlab.com"
val DOCKER_REGISTRY_IMAGE = System.getenv("CI_REGISTRY_IMAGE") ?: "${DOCKER_REGISTRY}/josm/docker-library"
val DOCKER_REGISTRY_USER: String by lazy {
  requireNotNull(System.getenv("CI_REGISTRY_USER")) {
    "Docker registry user is not set! Missing env var `CI_REGISTRY_USER`."
  }
}
val DOCKER_REGISTRY_PASSWORD: String by lazy {
  requireNotNull(System.getenv("CI_REGISTRY_PASSWORD")) {
    "Docker registry password is not set! Missing env var `CI_REGISTRY_PASSWORD`."
  }
}

class DockerDir(dockerfile: File) {
  // absolute File referencing the directory that contains the Dockerfile (e.g. /home/user/docker-library/src/eclipse/temurin/21)
  val dockerDir: File = dockerfile.parentFile.absoluteFile
  // path segments from /src/ to the directory that contains the Dockerfile (e.g. listOf("eclipse", "temurin", "21"))
  val pathSegments: List<String> = dockerfile.parentFile.relativeTo(projectDir.resolve("src")).invariantSeparatorsPath.split("/")
  // e.g. "eclipse-temurin"
  val imageName: String = pathSegments.dropLast(1).joinToString("-")
  // e.g. "21"
  val imageVersion: String = pathSegments.last()

  // full tag, e.g. "registry.gitlab.com/"
  val tag = "${DOCKER_REGISTRY_IMAGE}/$imageName:$imageVersion"

  val platforms: List<String> by lazy(LazyThreadSafetyMode.PUBLICATION) {
    (
      dockerDir.parentFile.resolve("platform-$imageVersion.txt")
        .takeIf { it.canRead() }
        ?: throw InvalidUserDataException("No platforms found for $imageName:$imageVersion ${dockerDir.parentFile.resolve("platform-$imageVersion.txt")}")
    )
      .readText()
      .trim()
      .split(Regex("[^a-zA-Z0-9/]+"))
      .filter { it.isNotBlank() }
      .takeIf { it.isNotEmpty() }
      ?: throw InvalidUserDataException("Platform definition for $imageName:$imageVersion seems empty!")
  }

  fun taskName(baseName: String) = baseName + pathSegments.joinToString("") { it.lowercase().replaceFirstChar { it.uppercaseChar() } }
}

val dockerDirs = project.fileTree(".")
  .apply {
    include("src/**")
  }
  .files
  .filter { it.name == "Dockerfile" }
  .map {
    DockerDir(it)
  }

tasks {
  val dockerLogin by registering(Exec::class) {
    group = "Docker"
    doFirst {
      commandLine("docker", "login", DOCKER_REGISTRY, "-u", DOCKER_REGISTRY_USER, "--password-stdin")
      standardInput = DOCKER_REGISTRY_PASSWORD.byteInputStream()
    }
  }

  val dockerCreateContext by registering(Exec::class) {
    group = "Docker"
    commandLine("docker", "context", "create", "builder")
    isIgnoreExitValue = true
  }

  val dockerBuildxCreate by registering(Exec::class) {
    group = "Docker"
    dependsOn(dockerCreateContext)
    commandLine("docker", "buildx", "create", "builder", "--use")
  }

  val buildTasks = dockerDirs.map { dockerDir ->
    register(dockerDir.taskName("build"), Exec::class) {
      group = "Docker Build"
      description = "Build image ${dockerDir.tag} in ${dockerDir.dockerDir.relativeTo(projectDir).path}"
      dependsOn(dockerBuildxCreate)
      commandLine("docker", "buildx", "build",
        "--tag", dockerDir.tag,
        "--load",
        dockerDir.dockerDir.absolutePath,
      )
    }
  }
  val build by registering {
    group = "Docker Build"
    description = "Build all available images"
    dependsOn(buildTasks)
  }

  val pushTasks = dockerDirs.map { dockerDir ->
    register(dockerDir.taskName("push"), Exec::class) {
      group = "Docker Push"
      description = "Build image ${dockerDir.tag} in ${dockerDir.dockerDir.relativeTo(projectDir).path} and publish to registry"
      dependsOn(dockerBuildxCreate, dockerLogin)
      commandLine("docker", "buildx", "build",
        "--tag", dockerDir.tag,
        "--platform", dockerDir.platforms.joinToString(","),
        "--push",
        dockerDir.dockerDir.absolutePath,
      )
    }
  }
  val push by registering {
    group = "Docker Push"
    description = "Build all available images and push them to ${DOCKER_REGISTRY_IMAGE}"
    dependsOn(pushTasks)
  }
}
