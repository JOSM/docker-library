# docker library for JOSM-related projects

This project provides Docker images intended for building JOSM plugins and other JOSM-related software.

There are images available for the LTS Java versions 8, 11, 17 and 21:
* [`registry.gitlab.com/josm/docker-library/eclipse-temurin:8`](./src/eclipse/temurin/8/Dockerfile)
* [`registry.gitlab.com/josm/docker-library/eclipse-temurin:11`](./src/eclipse/temurin/11/Dockerfile)
* [`registry.gitlab.com/josm/docker-library/eclipse-temurin:17`](./src/eclipse/temurin/17/Dockerfile)
* [`registry.gitlab.com/josm/docker-library/eclipse-temurin:21`](./src/eclipse/temurin/21/Dockerfile)

For more details on the image see its [README file](./src/eclipse/temurin/README.md).

## Development

To build the images locally, either run the Docker commands locally:
```shell
./gradlew build # This will internally run `docker buildx build`
```

Or run the build with Docker Compose:
```shell
docker compose up
```
